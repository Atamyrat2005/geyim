<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class VerificationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $request->validate([
            'q' => 'nullable|string|max:255',
        ]);
        $q = $request->q ?: null;

        $objs = Verification::when($q, function ($query, $q) {
            return $query->where(function ($query) use ($q) {
                $query->orWhere('phone', 'like', '%' . $q . '%');
                $query->orWhere('code', 'like', '%' . $q . '%');
            });
        })
            ->orderBy('id', 'desc')
            ->paginate(50)
            ->withQueryString();

        return view('admin.verification.index')
            ->with([
                'objs' => $objs,
            ]);
    }
}
