<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Brand;
use Illuminate\Http\Request;

class BrandController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $objs = Brand::orderBy('name')
            ->withCount([
                'products as in_stock_products_count' => function ($query) {
                    $query->where('stock', '>', 0);
                }, 'products as out_of_stock_products_count' => function ($query) {
                    $query->where('stock', '<', 1);
                }
            ])
            ->get();

        return view('admin.brand.index')
            ->with([
                'objs' => $objs,
            ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.brand.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'image' => ['nullable', 'image', 'mimes:png', 'max:16', 'dimensions:width=200,height=200'],
        ]);

        $obj = Brand::create([
            'name' => $request->name,
        ]);

        if ($request->hasFile('image')) {
            $name = str()->random(10) . '.' . $request->image->extension();
            Storage::putFileAs('public/b', $request->image, $name);
            $obj->image = $name;
            $obj->update();
        }

        return to_route('admin.brands.index')
            ->with([
                'success' => trans('app.brand') . ' (' . $obj->getName() . ') ' . trans('app.added') . '!'
            ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $obj = Brand::findOrFail($id);

        return view('admin.brand.edit')
            ->with([
                'obj' => $obj,
            ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'image' => ['nullable', 'image', 'mimes:png', 'max:16', 'dimensions:width=200,height=200'],
        ]);

        $obj = Brand::updateOrCreate([
            'id' => $id,
        ], [
            'name' => $request->name,
        ]);

        if ($request->hasFile('image')) {
            if ($obj->image) {
                Storage::delete('public/b/' . $obj->image);
            }
            $name = str()->random(10) . '.' . $request->image->extension();
            Storage::putFileAs('public/b', $request->image, $name);
            $obj->image = $name;
            $obj->update();
        }

        return to_route('admin.brands.index')
            ->with([
                'success' => trans('app.brand') . ' (' . $obj->getName() . ') ' . trans('app.updated') . '!'
            ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $obj = Brand::withCount('products')
            ->findOrFail($id);
        $objName = $obj->name;
        if ($obj->products_count > 0) {
            return redirect()->back()
                ->with([
                    'error' => trans('app.error') . '!'
                ]);
        }
        $obj->delete();

        return redirect()->back()
            ->with([
                'success' => trans('app.brand') . ' (' . $objName . ') ' . trans('app.deleted') . '!'
            ]);
    }
}
