<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Order;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $request->validate([
            'q' => 'nullable|string|max:255',
            'platform' => 'nullable|integer|between:0,3',
            'language' => 'nullable|integer|between:0,2',
            'payment' => 'nullable|integer|between:0,3',
            'status' => 'nullable|integer|between:0,4',
        ]);
        $q = $request->q ?: null;
        $f_platform = $request->has('platform') ? $request->platform : null;
        $f_language = $request->has('language') ? $request->language : null;
        $f_payment = $request->has('payment') ? $request->payment : null;
        $f_status = $request->has('status') ? $request->status : null;

        $objs = Order::when($q, function ($query, $q) {
            return $query->where(function ($query) use ($q) {
                $query->orWhere('code', 'like', '%' . $q . '%');
                $query->orWhere('customer_name', 'like', '%' . $q . '%');
                $query->orWhere('customer_phone', 'like', '%' . $q . '%');
                $query->orWhere('customer_address', 'like', '%' . $q . '%');
                $query->orWhere('customer_note', 'like', '%' . $q . '%');
            });
        })
            ->when(isset($f_platform), function ($query) use ($f_platform) {
                return $query->where('platform', $f_platform);
            })
            ->when(isset($f_language), function ($query) use ($f_language) {
                return $query->where('language', $f_language);
            })
            ->when(isset($f_payment), function ($query) use ($f_payment) {
                return $query->where('payment', $f_payment);
            })
            ->when(isset($f_status), function ($query) use ($f_status) {
                return $query->where('status', $f_status);
            })
            ->orderBy('id', 'desc')
            ->with(['location.parent', 'customer'])
            ->withCount(['orderProducts'])
            ->paginate(50)
            ->withQueryString();

        return view('admin.order.index')
            ->with([
                'objs' => $objs,
            ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
