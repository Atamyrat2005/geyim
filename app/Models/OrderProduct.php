<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OrderProduct extends Model
{
    use HasFactory;

    protected $guarded = [
        'id',
    ];

    protected $casts = [
        'created_at' => 'datetime',
    ];

    const UPDATED_AT = null;


    public function order()
    {
        return $this->belongsTo(Order::class)
            ->withTrashed();
    }


    public function product()
    {
        return $this->belongsTo(Product::class);
    }


    public function updatePrice()
    {
        $this->total_price = round($this->quantity * $this->price * (1 - $this->discount_percent / 100), 1);
        $this->update();
    }
}
