<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Order extends Model
{
    use HasFactory, SoftDeletes;

    protected $guarded = [
        'id',
    ];

    protected $casts = [
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
    ];


    public function location()
    {
        return $this->belongsTo(Location::class);
    }


    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }


    public function orderProducts()
    {
        return $this->hasMany(OrderProduct::class)
            ->orderBy('id');
    }


    public function status()
    {
        if ($this->status == 4) {
            return trans('app.canceled');
        } elseif ($this->status == 3) {
            return trans('app.completed');
        } elseif ($this->status == 2) {
            return trans('app.sent');
        } elseif ($this->status == 1) {
            return trans('app.accepted');
        } else {
            return trans('app.pending');
        }
    }


    public function statusColor()
    {
        if ($this->status == 4) {
            return 'danger';
        } elseif ($this->status == 3) {
            return 'success';
        } elseif ($this->status == 2) {
            return 'info';
        } elseif ($this->status == 1) {
            return 'info';
        } else {
            return 'warning';
        }
    }


    public function platform()
    {
        switch ($this->platform) {
            case 1:
                return 'Android';
                break;
            case 2:
                return 'iOS';
                break;
            default:
                return 'Web';
        }
    }


    public function language()
    {
        switch ($this->language) {
            case 1:
                return 'eng';
            default:
                return 'tkm';
        }
    }


    public function payment()
    {
        switch ($this->payment) {
            case 1:
                return trans('app.terminalPayment');
                break;
            case 2:
                return trans('app.onlinePayment');
                break;
            default:
                return trans('app.cashPayment');
        }
    }


    public function updatePrice()
    {
        $totalPrice = 0;
        foreach ($this->orderProducts as $product) {
            $totalPrice += $product->total_price;
        }
        $this->total_price = round($totalPrice, 1);
        $this->update();
    }
}
