@extends('admin.layouts.app')
@section('title')
    @lang('app.orders')
@endsection
@section('content')
    <div class="d-flex justify-content-between align-items-center mb-3">
        <div class="h4 mb-0">
            @lang('app.orders')
        </div>
        <div>
            @include('admin.order.filter')
        </div>
    </div>

    <div class="table-responsive">
        <table class="table table-hover table-striped">
            <thead>
            <tr>
                <th scope="col">ID</th>
                <th scope="col">Code</th>
                <th scope="col" width="30%">Location</th>
                <th scope="col">Customer</th>
                <th scope="col">Price</th>
                <th scope="col">Status</th>
            </tr>
            </thead>
            <tbody>
            @foreach($objs as $obj)
                <tr>
                    <td>{{ $obj->id }}</td>
                    <td>
                        <a href="{{ route('admin.orders.show', $obj->id) }}" class="text-decoration-none">
                            <span class="font-monospace">{{ $obj->code }}</span>
                            <i class="bi-box-arrow-up-right"></i>
                        </a>
                        <div class="small text-secondary">
                            <img src="{{ asset('img/flag/' . $obj->language() . '.png') }}" alt="Language" height="12" class="mb-1">
                            {{ $obj->platform() }}
                        </div>
                    </td>
                    <td>
                        <div class="mb-1">
                            <i class="bi-geo-alt-fill text-secondary"></i>
                            @if($obj->location->parent_id)
                                {{ $obj->location->parent->getName() }},
                            @endif
                            {{ $obj->location->getName() }}
                        </div>
                        <div class="small">
                            <i class="bi-geo-fill text-secondary"></i> {{ $obj->customer_address }}
                        </div>
                        @if($obj->customer_note)
                            <div class="small">
                                <i class="bi-sticky-fill text-warning"></i> {{ $obj->customer_note }}
                            </div>
                        @endif
                    </td>
                    <td>
                        @if($obj->customer_id)
                            <div class="mb-1">
                                <i class="bi-person-square text-secondary"></i>
                                <a href="{{ route('admin.customers.show', $obj->customer_id) }}" class="text-decoration-none">
                                    {{ $obj->customer_name }}
                                    @if($obj->customer_name != $obj->customer->name)
                                        ({{ $obj->customer->name }})
                                    @endif
                                    <i class="bi-box-arrow-up-right"></i>
                                </a>
                            </div>
                        @else
                            <div class="mb-1">
                                <i class="bi-person-square text-secondary"></i>
                                {{ $obj->customer_name }}
                            </div>
                        @endif
                        <div>
                            <i class="bi-telephone-fill text-success"></i>
                            <a href="tel:+993{{ $obj->customer_phone }}" class="text-decoration-none">
                                +993 {{ $obj->customer_phone }}
                            </a>
                        </div>
                    </td>
                    <td>
                        <div class="fs-5 mb-1">
                            {{ number_format($obj->total_price, 2, '.', ' ') }}
                            <small>TMT</small>
                        </div>
                        <div class="text-danger fw-semibold">
                            <i class="bi-box-fill text-secondary"></i> {{ $obj->order_products_count }}
                        </div>
                    </td>
                    <td>
                        <div class="mb-1">
                            <span class="badge text-bg-{{ $obj->statusColor() }}">{{ $obj->status() }}</span>
                        </div>
                        <div>{{ $obj->payment() }}</div>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection